package edu.itstep.zno_accouting.controller;

import edu.itstep.zno_accouting.models.Exam;
import edu.itstep.zno_accouting.models.Student;
import edu.itstep.zno_accouting.models.StudentExam;
import edu.itstep.zno_accouting.repositories.ExamRepository;
import edu.itstep.zno_accouting.repositories.StudentRepository;
import edu.itstep.zno_accouting.repositories.StudentExamRepository;

public class MainController {
    private ExamRepository examRepository;
    private StudentRepository studentRepository;
    private StudentExamRepository studentExamRepository;

    public MainController(ExamRepository examRepository, StudentRepository studentRepository, StudentExamRepository studentExamRepository) {
        this.examRepository = examRepository;
        this.studentRepository = studentRepository;
        this.studentExamRepository = studentExamRepository;
    }

    public void addExam(Exam exam) {
        if (!examRepository.existsByName(exam.getName())) {
            examRepository.save(exam);
        } else {
            System.out.println("Іспит з такою назвою вже існує.");
        }
    }

    public void addStudent(Student student) {
        if (!studentRepository.existsByFirstNameAndLastName(student.getFirstName(), student.getLastName())) {
            studentRepository.save(student);
        } else {
            System.out.println("Студент з таким ім'ям та прізвищем вже існує.");
        }
    }

    public void addExamToStudent(Student student, Exam exam) {
        if (!studentExamRepository.existsByStudentAndExam(student, exam)) {
            StudentExam studentExam = new StudentExam();
            studentExam.setStudent(student);
            studentExam.setExam(exam);
            studentExamRepository.save(studentExam);
        } else {
            System.out.println("Студент вже записаний на цей іспит.");
        }
    }
}
