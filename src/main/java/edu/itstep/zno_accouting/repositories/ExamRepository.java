package edu.itstep.zno_accouting.repositories;
import edu.itstep.zno_accouting.models.Exam;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;

public class ExamRepository {

    private SessionFactory sessionFactory;

    public ExamRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Exam exam) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(exam);
        session.getTransaction().commit();
        session.close();
    }

    public List<Exam> findAll() {
        Session session = sessionFactory.openSession();
        List<Exam> exams = session.createQuery("from Exam", Exam.class).list();
        session.close();
        return exams;
    }

    public boolean existsByName(String name) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from Exam where name = :name");
        query.setParameter("name", name);
        boolean exists = !query.getResultList().isEmpty();
        session.close();
        return exists;
    }

}