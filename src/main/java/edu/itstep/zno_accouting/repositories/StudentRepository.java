package edu.itstep.zno_accouting.repositories;

import edu.itstep.zno_accouting.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;

public class StudentRepository {
    private SessionFactory sessionFactory;

    public StudentRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Student student) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(student);
        session.getTransaction().commit();
        session.close();
    }

    public List<Student> findAll() {
        Session session = sessionFactory.openSession();
        List<Student> students = session.createQuery("from Student", Student.class).list();
        session.close();
        return students;
    }

    public boolean existsByFirstNameAndLastName(String firstName, String lastName) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from Student where firstName = :firstName and lastName = :lastName");
        query.setParameter("firstName", firstName);
        query.setParameter("lastName", lastName);
        boolean exists = !query.getResultList().isEmpty();
        session.close();
        return exists;
    }
}
