package edu.itstep.zno_accouting.repositories;

import edu.itstep.zno_accouting.models.Exam;
import edu.itstep.zno_accouting.models.Student;
import edu.itstep.zno_accouting.models.StudentExam;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;

public class StudentExamRepository {
    private final SessionFactory sessionFactory;

    public StudentExamRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(StudentExam studentExam) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(studentExam);
        session.getTransaction().commit();
        session.close();
    }

    public List<StudentExam> findAll() {
        Session session = sessionFactory.openSession();
        List<StudentExam> studentExams = session.createQuery("from StudentExam", StudentExam.class).list();
        session.close();
        return studentExams;
    }

    public boolean existsByStudentAndExam(Student student, Exam exam) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from StudentExam where student = :student and exam = :exam");
        query.setParameter("student", student);
        query.setParameter("exam", exam);
        boolean exists = !query.getResultList().isEmpty();
        session.close();
        return exists;
    }
}
