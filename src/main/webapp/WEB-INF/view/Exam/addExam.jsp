<%--
  Created by IntelliJ IDEA.
  User: edena
  Date: 13.04.2024
  Time: 21:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Додати іспит</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<h1>Додати іспит</h1>
<form id="addExamForm">
    <label for="name">Назва:</label><br>
    <input type="text" id="name" name="name"><br>
    <label for="dateTime">Дата та час:</label><br>
    <input type="datetime-local" id="dateTime" name="dateTime"><br>
    <label for="address">Адреса:</label><br>
    <input type="text" id="address" name="address"><br>
    <input type="submit" value="Додати іспит">
</form>

<script>
    $(document).ready(function(){
        $("#addExamForm").submit(function(event){
            event.preventDefault();
            var name = $("#name").val();
            var dateTime = $("#dateTime").val();
            var address = $("#address").val();
            $.ajax({
                url: '/addExam',
                type: 'post',
                data: {name: name, dateTime: dateTime, address: address},
                success: function(response){
                    alert(response);
                    window.location.href = 'studentExams.jsp';
                }
            });
        });
    });
</script>
</body>
</html>
