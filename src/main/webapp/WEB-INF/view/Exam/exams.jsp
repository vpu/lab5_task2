<%--
  Created by IntelliJ IDEA.
  User: edena
  Date: 13.04.2024
  Time: 21:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Іспити</title>
</head>
<body>
<h1>Іспити</h1>
<table>
    <tr>
        <th>Назва</th>
        <th>Дата</th>
        <th>Адреса</th>
    </tr>
    <c:forEach var="exam" items="${exams}">
        <tr>
            <td>${exam.name}</td>
            <td>${exam.dateTime}</td>
            <td>${exam.address}</td>
        </tr>
    </c:forEach>
</table>
<button onclick="location.href='addExam.jsp'">Додати іспит</button>
<button onclick="location.href='studentExams.jsp'">Повернутися до головної сторінки</button>
</body>
</html>

