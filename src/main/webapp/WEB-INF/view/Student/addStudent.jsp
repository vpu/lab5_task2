<%--
  Created by IntelliJ IDEA.
  User: edena
  Date: 13.04.2024
  Time: 21:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Додати студента</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<h1>Додати студента</h1>
<form id="addStudentForm">
    <label for="firstName">Ім'я:</label><br>
    <input type="text" id="firstName" name="firstName"><br>
    <label for="lastName">Прізвище:</label><br>
    <input type="text" id="lastName" name="lastName"><br>
    <label for="schoolNumber">Номер школи:</label><br>
    <input type="text" id="schoolNumber" name="schoolNumber"><br>
    <input type="submit" value="Додати студента">
</form>

<script>
    $(document).ready(function(){
        $("#addStudentForm").submit(function(event){
            event.preventDefault();
            var firstName = $("#firstName").val();
            var lastName = $("#lastName").val();
            var schoolNumber = $("#schoolNumber").val();
            $.ajax({
                url: '/addStudent',
                type: 'post',
                data: {firstName: firstName, lastName: lastName, schoolNumber: schoolNumber},
                success: function(response){
                    alert(response);
                    window.location.href = 'studentExams.jsp';
                }
            });
        });
    });
</script>
</body>
</html>

