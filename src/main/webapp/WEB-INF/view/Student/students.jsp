<%--
  Created by IntelliJ IDEA.
  User: edena
  Date: 13.04.2024
  Time: 21:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Студенти</title>
</head>
<body>
<h1>Студенти</h1>
<table>
    <tr>
        <th>Ім'я</th>
        <th>Прізвище</th>
        <th>Номер школи</th>
    </tr>
    <c:forEach var="student" items="${students}">
        <tr>
            <td>${student.firstName}</td>
            <td>${student.lastName}</td>
            <td>${student.schoolNumber}</td>
        </tr>
    </c:forEach>
</table>
<button onclick="location.href='addStudent.jsp'">Додати студента</button>
<button onclick="location.href='studentExams.jsp'">Повернутися до головної сторінки</button>
</body>
</html>

