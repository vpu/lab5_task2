<%--
  Created by IntelliJ IDEA.
  User: edena
  Date: 13.04.2024
  Time: 21:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Записи студентів на іспити</title>
</head>
<body>
<h1>Записи студентів на іспити</h1>
<table>
    <tr>
        <th>Ім'я студента</th>
        <th>Прізвище студента</th>
        <th>Назва іспиту</th>
    </tr>
    <c:forEach var="studentExam" items="${studentExams}">
        <tr>
            <td>${studentExam.student.firstName}</td>
            <td>${studentExam.student.lastName}</td>
            <td>${studentExam.exam.name}</td>
        </tr>
    </c:forEach>
</table>
<button onclick="location.href='addStudentExam.jsp'">Записати студента на іспит</button>
<button onclick="location.href='Student/students.jsp'">Переглянути усіх студентів</button>
<button onclick="location.href='Exam/exams.jsp'">Переглянути усі іспити</button>

<button onclick="location.href='studentExams.jsp'">Повернутися до головної сторінки</button>
</body>
</html>

