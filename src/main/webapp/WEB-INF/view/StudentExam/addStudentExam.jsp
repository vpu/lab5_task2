<%--
  Created by IntelliJ IDEA.
  User: edena
  Date: 13.04.2024
  Time: 21:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Записати студента на іспит</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<h1>Записати студента на іспит</h1>
<form id="addStudentExamForm">
    <label for="studentId">ID студента:</label><br>
    <input type="text" id="studentId" name="studentId"><br>
    <label for="examId">ID іспиту:</label><br>
    <input type="text" id="examId" name="examId"><br>
    <input type="submit" value="Записати студента на іспит">
</form>

<script>
    $(document).ready(function(){
        $("#addStudentExamForm").submit(function(event){
            event.preventDefault();
            var studentId = $("#studentId").val();
            var examId = $("#examId").val();
            $.ajax({
                url: '/addStudentExam',
                type: 'post',
                data: {studentId: studentId, examId: examId},
                success: function(response){
                    alert(response);
                    window.location.href = 'studentExams.jsp';
                }
            });
        });
    });
</script>
</body>
</html>

